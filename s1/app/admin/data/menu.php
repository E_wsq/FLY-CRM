[
    {
        "name": "系统管理",
        "sort": 1000,
        "module": "admin",
        "url": "system admin",
        "visible": 1,
        "is_shortcut": 0,
        "is_menu": 1,
        "icon": "fa-wrench",
        "nodes": [
            {
                "name": "行政区域",
                "sort": 5,
                "module": "admin",
                "url": "Region/show",
                "visible": 1,
                "is_shortcut": 0,
                "is_menu": 1,
                "icon": "",
                "nodes": [
                    {
                        "name": "浏览",
                        "sort": 1,
                        "module": "admin",
                        "url": "Region/show_json",
                        "visible": 1,
                        "is_shortcut": 0,
                        "is_menu": 0,
                        "icon": "",
                        "nodes": []
                    }
                ]
            },
            {
                "name": "地区管理",
                "sort": 2,
                "module": "admin",
                "url": "SysArea/show",
                "visible": 1,
                "is_shortcut": 0,
                "is_menu": 1,
                "icon": "",
                "nodes": [
                    {
                        "name": "设置负责人",
                        "sort": 1,
                        "module": "admin",
                        "url": "SysArea/manage",
                        "visible": 1,
                        "is_shortcut": 0,
                        "is_menu": 0,
                        "icon": "",
                        "nodes": []
                    },
                    {
                        "name": "排序",
                        "sort": 1,
                        "module": "admin",
                        "url": "SysArea/set_sort",
                        "visible": 1,
                        "is_shortcut": 0,
                        "is_menu": 0,
                        "icon": "",
                        "nodes": []
                    },
                    {
                        "name": "启用",
                        "sort": 1,
                        "module": "admin",
                        "url": "SysArea/set_visible",
                        "visible": 1,
                        "is_shortcut": 0,
                        "is_menu": 0,
                        "icon": "",
                        "nodes": []
                    },
                    {
                        "name": "修改",
                        "sort": 1,
                        "module": "admin",
                        "url": "SysArea/edit",
                        "visible": 1,
                        "is_shortcut": 0,
                        "is_menu": 0,
                        "icon": "",
                        "nodes": []
                    },
                    {
                        "name": "删除",
                        "sort": 1,
                        "module": "admin",
                        "url": "SysArea/del",
                        "visible": 1,
                        "is_shortcut": 0,
                        "is_menu": 0,
                        "icon": "",
                        "nodes": []
                    },
                    {
                        "name": "添加",
                        "sort": 1,
                        "module": "admin",
                        "url": "SysArea/add",
                        "visible": 1,
                        "is_shortcut": 0,
                        "is_menu": 0,
                        "icon": "",
                        "nodes": []
                    },
                    {
                        "name": "浏览",
                        "sort": 1,
                        "module": "admin",
                        "url": "SysArea/show_json",
                        "visible": 1,
                        "is_shortcut": 0,
                        "is_menu": 0,
                        "icon": "",
                        "nodes": []
                    }
                ]
            },
            {
                "name": "行为日志",
                "sort": 4,
                "module": "admin",
                "url": "Log/show",
                "visible": 1,
                "is_shortcut": 0,
                "is_menu": 1,
                "icon": "fa-street-view",
                "nodes": []
            },
            {
                "name": "系统设置",
                "sort": 1,
                "module": "admin",
                "url": "config/setting",
                "visible": 1,
                "is_shortcut": 0,
                "is_menu": 1,
                "icon": "",
                "nodes": []
            },
            {
                "name": "配置列表",
                "sort": 2,
                "module": "admin",
                "url": "config/configlist",
                "visible": 1,
                "is_shortcut": 0,
                "is_menu": 1,
                "icon": "",
                "nodes": [
                    {
                        "name": "添加",
                        "sort": 81,
                        "module": "admin",
                        "url": "Config/configAdd",
                        "visible": 1,
                        "is_shortcut": 0,
                        "is_menu": 0,
                        "icon": "",
                        "nodes": []
                    },
                    {
                        "name": "编辑",
                        "sort": 1,
                        "module": "admin",
                        "url": "Config/configEdit",
                        "visible": 1,
                        "is_shortcut": 0,
                        "is_menu": 0,
                        "icon": "",
                        "nodes": []
                    }
                ]
            },
            {
                "name": "数据库管理",
                "sort": 10,
                "module": "admin",
                "url": "Database/show",
                "visible": 1,
                "is_shortcut": 0,
                "is_menu": 1,
                "icon": "fa-database",
                "nodes": [
                    {
                        "name": "数据恢复",
                        "sort": 2,
                        "module": "admin",
                        "url": "Database/dataRestore",
                        "visible": 1,
                        "is_shortcut": 0,
                        "is_menu": 1,
                        "icon": "",
                        "nodes": []
                    },
                    {
                        "name": "数据备份",
                        "sort": 1,
                        "module": "admin",
                        "url": "Database/databackup",
                        "visible": 1,
                        "is_shortcut": 0,
                        "is_menu": 1,
                        "icon": "",
                        "nodes": []
                    }
                ]
            },
            {
                "name": "系统菜单",
                "sort": 3,
                "module": "admin",
                "url": "SysMenu/show",
                "visible": 1,
                "is_shortcut": 0,
                "is_menu": 1,
                "icon": "fa-user",
                "nodes": []
            },
            {
                "name": "组织结构",
                "sort": 9,
                "module": "admin",
                "url": "index/main",
                "visible": 1,
                "is_shortcut": 0,
                "is_menu": 1,
                "icon": "fa-sitemap",
                "nodes": [
                    {
                        "name": "企业会员",
                        "sort": 1,
                        "module": "admin",
                        "url": "SysOrg/show",
                        "visible": 1,
                        "is_shortcut": 0,
                        "is_menu": 1,
                        "icon": "",
                        "nodes": [
                            {
                                "name": "初始员工",
                                "sort": 5,
                                "module": "admin",
                                "url": "SysOrg/create_user",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            },
                            {
                                "name": "密码重置",
                                "sort": 100,
                                "module": "admin",
                                "url": "SysOrg/reset_pwd",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            },
                            {
                                "name": "企业修改",
                                "sort": 100,
                                "module": "admin",
                                "url": "SysOrg/edit",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            },
                            {
                                "name": "企业删除",
                                "sort": 100,
                                "module": "admin",
                                "url": "SysOrg/del",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            }
                        ]
                    },
                    {
                        "name": "部门管理",
                        "sort": 2,
                        "module": "admin",
                        "url": "SysDept/show",
                        "visible": 1,
                        "is_shortcut": 0,
                        "is_menu": 1,
                        "icon": "",
                        "nodes": [
                            {
                                "name": "删除",
                                "sort": 1,
                                "module": "admin",
                                "url": "SysDept/del",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            },
                            {
                                "name": "修改",
                                "sort": 1,
                                "module": "admin",
                                "url": "SysDept/edit",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            },
                            {
                                "name": "添加",
                                "sort": 1,
                                "module": "admin",
                                "url": "SysDept/add",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            },
                            {
                                "name": "浏览",
                                "sort": 1,
                                "module": "admin",
                                "url": "SysDept/show_json",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            }
                        ]
                    },
                    {
                        "name": "系统用户",
                        "sort": 3,
                        "module": "admin",
                        "url": "SysUser/show",
                        "visible": 1,
                        "is_shortcut": 0,
                        "is_menu": 1,
                        "icon": "fa-circle-o",
                        "nodes": [
                            {
                                "name": "菜单授权",
                                "sort": 2,
                                "module": "admin",
                                "url": "SysUser/userRules",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            },
                            {
                                "name": "用户浏览",
                                "sort": 2,
                                "module": "admin",
                                "url": "SysUser/show_json",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            },
                            {
                                "name": "用户授权",
                                "sort": 1,
                                "module": "admin",
                                "url": "SysUser/userAuth",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            },
                            {
                                "name": "用户修改",
                                "sort": 10,
                                "module": "admin",
                                "url": "SysUser/edit",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "fa-circle-o",
                                "nodes": []
                            },
                            {
                                "name": "用户删除",
                                "sort": 10,
                                "module": "admin",
                                "url": "SysUser/del",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "fa-circle-o",
                                "nodes": []
                            },
                            {
                                "name": "用户添加",
                                "sort": 10,
                                "module": "admin",
                                "url": "SysUser/add",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "fa-circle-o",
                                "nodes": []
                            }
                        ]
                    },
                    {
                        "name": "权限列表",
                        "sort": 3,
                        "module": "admin",
                        "url": "SysAuth/show",
                        "visible": 1,
                        "is_shortcut": 0,
                        "is_menu": 1,
                        "icon": "fa-circle-o",
                        "nodes": [
                            {
                                "name": "菜单授权",
                                "sort": 2,
                                "module": "admin",
                                "url": "SysAuth/MenuAuth",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            },
                            {
                                "name": "权限删除",
                                "sort": 2,
                                "module": "admin",
                                "url": "SysAuth/del",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            },
                            {
                                "name": "权限修改",
                                "sort": 2,
                                "module": "admin",
                                "url": "SysAuth/edit",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            },
                            {
                                "name": "权限添加",
                                "sort": 2,
                                "module": "admin",
                                "url": "SysAuth/add",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            },
                            {
                                "name": "权限浏览",
                                "sort": 2,
                                "module": "admin",
                                "url": "SysAuth/show_json",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            }
                        ]
                    },
                    {
                        "name": "职位管理",
                        "sort": 4,
                        "module": "admin",
                        "url": "SysPosition/show",
                        "visible": 1,
                        "is_shortcut": 0,
                        "is_menu": 1,
                        "icon": "",
                        "nodes": [
                            {
                                "name": "禁用",
                                "sort": 5,
                                "module": "admin",
                                "url": "SysPosition/set_visible",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            },
                            {
                                "name": "数据浏览",
                                "sort": 6,
                                "module": "admin",
                                "url": "SysPosition/show_json",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            },
                            {
                                "name": "树形数据浏览",
                                "sort": 7,
                                "module": "admin",
                                "url": "SysPosition/get_list_tree",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            },
                            {
                                "name": "添加",
                                "sort": 1,
                                "module": "admin",
                                "url": "SysPosition/add",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            },
                            {
                                "name": "修改",
                                "sort": 2,
                                "module": "admin",
                                "url": "SysPosition/edit",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            },
                            {
                                "name": "删除",
                                "sort": 3,
                                "module": "admin",
                                "url": "SysPosition/del",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            },
                            {
                                "name": "排序",
                                "sort": 4,
                                "module": "admin",
                                "url": "SysPosition/set_sort",
                                "visible": 1,
                                "is_shortcut": 0,
                                "is_menu": 0,
                                "icon": "",
                                "nodes": []
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        "name": "系统扩展",
        "sort": 1100,
        "module": "admin",
        "url": "system extend",
        "visible": 1,
        "is_shortcut": 0,
        "is_menu": 1,
        "icon": "fa-fire",
        "nodes": [
            {
                "name": "应用市场",
                "sort": 1,
                "module": "admin",
                "url": "Store/apps",
                "visible": 1,
                "is_shortcut": 0,
                "is_menu": 1,
                "icon": "",
                "nodes": [
                    {
                        "name": "会员中心",
                        "sort": 1,
                        "module": "admin",
                        "url": "Store/user",
                        "visible": 1,
                        "is_shortcut": 0,
                        "is_menu": 0,
                        "icon": "",
                        "nodes": []
                    }
                ]
            },
            {
                "name": "本地模块",
                "sort": 20,
                "module": "admin",
                "url": "SysModule/show",
                "visible": 1,
                "is_shortcut": 0,
                "is_menu": 1,
                "icon": "fa-user-secret",
                "nodes": []
            },
            {
                "name": "钩子列表",
                "sort": 40,
                "module": "admin",
                "url": "addon/hook_list",
                "visible": 1,
                "is_shortcut": 0,
                "is_menu": 1,
                "icon": "",
                "nodes": []
            },
            {
                "name": "插件列表",
                "sort": 30,
                "module": "admin",
                "url": "addon/addon_list",
                "visible": 1,
                "is_shortcut": 0,
                "is_menu": 1,
                "icon": "",
                "nodes": []
            },
            {
                "name": "框架升级",
                "sort": 4,
                "module": "admin",
                "url": "Upgrade/show",
                "visible": 1,
                "is_shortcut": 0,
                "is_menu": 1,
                "icon": "",
                "nodes": []
            },
            {
                "name": "服务管理",
                "sort": 50,
                "module": "admin",
                "url": "Service/serviceList",
                "visible": 1,
                "is_shortcut": 0,
                "is_menu": 1,
                "icon": "",
                "nodes": []
            }
        ]
    },
    {
        "name": "系统首页",
        "sort": 10,
        "module": "admin",
        "url": "index/main",
        "visible": 1,
        "is_shortcut": 0,
        "is_menu": 1,
        "icon": "",
        "nodes": [
            {
                "name": "密码修改",
                "sort": 13,
                "module": "admin",
                "url": "SysUser/editPwd",
                "visible": 1,
                "is_shortcut": 0,
                "is_menu": 0,
                "icon": "",
                "nodes": []
            },
            {
                "name": "资料修改",
                "sort": 12,
                "module": "admin",
                "url": "SysUser/editInfo",
                "visible": 1,
                "is_shortcut": 0,
                "is_menu": 0,
                "icon": "",
                "nodes": []
            }
        ]
    }
]